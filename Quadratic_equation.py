# Решение квадратного уравнения #

from math import sqrt
a = float(input('Enter a: '))
b = float(input('Enter b: '))
c = float(input('Enter c: '))

if a != 0:
    discriminant = b**2 - 4 * a * c
    if discriminant > 0:
        x_1 = (-b + sqrt(discriminant)) / (2 * a)
        x_2 = (-b - sqrt(discriminant)) / (2 * a)
        print(x_1)
        print(x_2)
    elif discriminant == 0:
        x = -b / (2 * a)
        print(x)
    else:
        print('No roots.')
else:
    print('Error: a = 0.')